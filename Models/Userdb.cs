﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using webapi.Models;

namespace webapi.Models
{
    public class Userdb
    {
        SqlConnection con = new SqlConnection("Server=LIFESEA\\LEPKOMF4;Database=db_crud;Persist Security Info=True;User ID=miksea;Password=miksea123;MultipleActiveResultSets=True;TrustServerCertificate=True;");

        public string saveRecord(User user)
        {
            try 
            {
                SqlCommand com = new SqlCommand("insert into [db_crud].[dbo].[User](FirstName,LastName,EmailAddress) values(@FirstName, @LastName, @EmailAddress)", con);
                com.Parameters.AddWithValue("@FirstName", user.FirstName);
                com.Parameters.AddWithValue("@LastName", user.LastName);
                com.Parameters.AddWithValue("@EmailAddress", user.EmailAddress);
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
                return ("OK");
            } catch(Exception ex) 
            {
                if(con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return (ex.Message.ToString());
            }
        }
    }
}
