﻿using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    public class HelperController : Controller
    {
        [Route("/helper/index")]
        public IActionResult Help()
        {
            return View();
        }
    }
}
