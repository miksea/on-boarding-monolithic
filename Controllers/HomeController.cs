using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using webapi.Models;

namespace webapi.Controllers
{
    public class HomeController : Controller
    {
        Userdb u = new Userdb();

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index([Bind] User users)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string res = u.saveRecord(users);
                    TempData["msg"] = res;
                }
            }catch(Exception ex)
            {
                TempData["msg"] = ex.Message;
            }
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
